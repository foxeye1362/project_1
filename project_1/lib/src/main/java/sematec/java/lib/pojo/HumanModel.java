package sematec.java.lib.pojo;

public class HumanModel {
    private String name;
    private String family;
    private int age;
    private String country;
    private String city ;
//    public void setAge(String pinCode, int age) {
//        if (pinCode.equals("5566"))
//            if (age > 0 && age < 110)
//                this.age = age;
//            else
//                this.age = 0;
//    }


    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    private int getNationalCode(){
        return 10 ;
    }

    public boolean isStudent(){

        if (getAge()>18 && getAge()<26)
            return true;
        return false ;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {

        this.name = name.trim().toLowerCase().toUpperCase()
                .replace("a" , "b");
    }



    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
