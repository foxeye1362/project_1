package sematec.java.lib;

public class OOPClass {

    private String getName(int age) {
        if (age > 10)
            return "Ali";

        return null;
    }

    String getValue() {
        return getName(10);
    }

    public static boolean isAlive(int age) {
        if (age > 100)
            return false;
        return true;
    }



}
