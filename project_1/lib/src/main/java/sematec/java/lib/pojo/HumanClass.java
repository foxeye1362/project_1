package sematec.java.lib.pojo;


import sematec.java.lib.PublicMethods;

public class HumanClass {

    public static void main(String[] args) {

        HumanModel ali = new HumanModel();
        // ali.setAge("5566" , 10);
        ali.setName("Ali");
        ali.setAge(9);
        ali.setCountry("Iran");
        ali.setFamily("Ahmadi");


        if (ali.isStudent())
            PublicMethods.print(ali.getName() +
                    " is a " +
                    (ali.isStudent() ? "student" : " not student")
                + "\n"

            );

        PublicMethods.print(ali.getName() + " is from "
                + ali.getCountry());

    }


}
